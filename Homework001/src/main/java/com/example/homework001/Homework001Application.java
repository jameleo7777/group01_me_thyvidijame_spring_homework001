package com.example.homework001;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@OpenAPIDefinition(info = @Info(title = "Customer REST API", description = "Welcome to Spring REST API", version = "v1"))
public class Homework001Application {

    public static void main(String[] args) {
        SpringApplication.run(Homework001Application.class, args);
    }

}
