package com.example.homework001.Controller;


import com.fasterxml.jackson.annotation.JsonInclude;

import java.time.LocalDateTime;

public class CustomerResponse <T>{
    private String message;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private T customer;
    private String status;;
    private LocalDateTime time;



    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public LocalDateTime getTime() {
        return time;
    }

    public void setTime(LocalDateTime time) {
        this.time = time;
    }

    public T getCustomer() {
        return customer;
    }

    public void setCustomer(T customer) {
        this.customer = customer;
    }

    public CustomerResponse(String message, T customer, String status, LocalDateTime time) {
        this.message = message;
        this.customer = customer;
        this.status = status;
        this.time = time;

    }

    public CustomerResponse(String message, String status, LocalDateTime time){
        this.message = message;
        this.status = status;
        this.time = time;
    }

    @Override
    public String toString() {
        return "Customer{" +
                "message='" + message + '\'' +
                ", payload=" + customer +
                ", status='" + status + '\'' +
                ", time=" + time +
                '}';
    }
}
