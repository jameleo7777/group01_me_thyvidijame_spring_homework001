package com.example.homework001.Controller;

import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/v1")
public class CustomerController {

    // Customer ArrayList store Customer
    private static List<Customer> customer = new ArrayList<>();


    // Default Customer
    static{
        customer.add(new Customer(1,"Jack","Male",25,"PP"));
        customer.add(new Customer(2,"Jimmy","Female",23,"BTB"));
        customer.add(new Customer(3,"Jame","Male",22,"BTB"));
    }
    static int count = 3;

    // Show Customer
    @GetMapping("/customer")

    public ResponseEntity<CustomerResponse>  getCustomer(){
        return ResponseEntity.ok(new CustomerResponse<>( "You're got all customer Successfully",customer,String.valueOf(OK()),LocalDateTime.now())
        );
    }
    // Insert Customer
    @PostMapping("/customer")
    public ResponseEntity<CustomerResponse> addCustomer(@RequestBody CustomerRequest customerRequest){
        customer.add(new Customer(++count,customerRequest.getName(),customerRequest.getGender(),customerRequest.getAge(),customerRequest.getAddress()));
       return ResponseEntity.ok(new CustomerResponse<>(
               "You're inserted successfully",
               new Customer(count,customerRequest.getName(),
                       customerRequest.getGender(),
                       customerRequest.getAge(),
                       customerRequest.getAddress()
                       ),OK(),LocalDateTime.now()


       ));
    }

    // Search By Customer ID
    @GetMapping("/customer/{id}")
    public ResponseEntity<CustomerResponse> getCustomerById(@PathVariable("id") Integer customerID, HttpServletResponse response){

        for(int i=0; i<customer.size();i++){
            if(customer.get(i).getId() == customerID){
                return ResponseEntity.ok(new CustomerResponse<>(
                        "This record has found successfully",
                        new Customer(customer.get(i).getId(),
                                customer.get(i).getName(),
                                customer.get(i).getGender(),
                                customer.get(i).getAge(),
                                customer.get(i).getAddress()
                                ),OK(),LocalDateTime.now()
                ));
            }else{
                response.setStatus(HttpServletResponse.SC_NOT_FOUND);
            }

        }
        return null;
    }

    // Search By Customer Name
    @GetMapping("/customer/search")
    public ResponseEntity<CustomerResponse> findCustomerByName(@RequestParam String name,HttpServletResponse response){
        for(Customer find : customer){
            if(find.getName().equals(name)  ){
                return ResponseEntity.ok(new CustomerResponse<>(
                        "This record has found successfully",
                        new Customer(find.getId(),
                                find.getName(),
                                find.getGender(),
                                find.getAge(),
                                find.getAddress()
                        ),OK(),LocalDateTime.now()
                ));
            }else{
                response.setStatus(HttpServletResponse.SC_NOT_FOUND);
            }
        }
        return null;
    }

    // Update By Customer ID
    @PutMapping("/customer/{id}")
    public ResponseEntity<?> updateCustomerById(@PathVariable("id") Integer updateID, @RequestBody CustomerRequest customerRequest, HttpServletResponse response){
        for(int i=0; i<customer.size(); i++){
            if(customer.get(i).getId() == updateID){
                customer.get(i).setName(customerRequest.getName());
                customer.get(i).setGender(customerRequest.getGender());
                customer.get(i).setAge(customerRequest.getAge());
                customer.get(i).setAddress(customerRequest.getAddress());

                return ResponseEntity.ok(new CustomerResponse<>(
                        "You have updated successfully",
                        new Customer(updateID,
                                customerRequest.getName(),
                                customerRequest.getGender(),
                                customerRequest.getAge(),
                                customerRequest.getAddress()
                        ),OK(),LocalDateTime.now()
                ));
            }else{
                response.setStatus(HttpServletResponse.SC_NOT_FOUND);
            }

        }
        return null;
    }

    @DeleteMapping("/customer/{id}")
    public ResponseEntity<CustomerResponse> deleteCustomerById(@PathVariable("id") Integer deleteID, HttpServletResponse response){
        for(int i=0; i<customer.size();i++){
            if(customer.get(i).getId() == deleteID){
                customer.remove(customer.get(i));
                return ResponseEntity.ok(new CustomerResponse("You have deleted successfully",null
                ,OK(),LocalDateTime.now()));




            }else{
                response.setStatus(HttpServletResponse.SC_NOT_FOUND);
            }
        }
        return null;
    }
    public static final String OK(){
        return HttpStatus.OK.getReasonPhrase();
    }
}
