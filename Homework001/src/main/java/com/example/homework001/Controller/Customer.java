package com.example.homework001.Controller;

public class Customer {

        private int id;
        private String name;
        private String gender;
        private int age;
        private String address;



        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }

        public int getAge() {
            return age;
        }

        public void setAge(int age) {
            this.age = age;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public Customer(int id,String name, String gender, int age, String address) {
            super();
            this.id = id;
            this.name = name;
            this.gender = gender;
            this.age = age;
            this.address = address;
        }

        public Customer(){
            super();
        }

    @Override
    public String toString() {
        return "Customer{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", gender='" + gender + '\'' +
                ", age=" + age +
                ", address='" + address + '\'' +
                '}';
    }
}
